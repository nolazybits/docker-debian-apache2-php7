#!/usr/bin/env bash
touch /etc/apt/sources.list.d/php7.list
echo "deb http://packages.dotdeb.org jessie all
deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list.d/php7.list
wget https://www.dotdeb.org/dotdeb.gpg
apt-key add dotdeb.gpg
apt-get update

apt-get -y --force-yes install \
        libapache2-mod-php7.0 \
        php-pear \
        php7.0 \
        php7.0-curl \
        php7.0-dev \
        php7.0-gd \
        php7.0-imap \
        php7.0-mysqlnd \
        php7.0-xdebug \
        php7.0-json \
        php7.0-memcached
