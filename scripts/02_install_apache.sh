#!/usr/bin/env bash

apt-get install -y --force-yes apache2
echo "ServerName localhost" | tee /etc/apache2/conf-available/fqdn.conf
sed -i 's/#ServerName www.example.com/ServerName localhost/g' /etc/apache2/sites-available/000-default.conf
a2enconf fqdn
a2enmod rewrite