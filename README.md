### Docker image with Apache 2.x and PHP 7.x based on Debian Jessie ###
[![](https://images.microbadger.com/badges/image/nolazybits/debian-apache-php7.svg)](http://microbadger.com/images/nolazybits/debian-apache-php7 "See the layers")

[![](https://images.microbadger.com/badges/version/nolazybits/debian-apache-php7.svg)](http://microbadger.com/images/nolazybits/debian-apache-php7)